#!/bin/bash 
#------------------------------------------------------------------------------------
# watch_folder.sh
# Launcher for Watch Folder script
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2019 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYTHONPATH=$SCRIPTPATH
$PYTHON $SCRIPTPATH/watch_folder/watch_folder.py -D $(pwd) $*
