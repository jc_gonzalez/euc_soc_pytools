#!/bin/bash 
#------------------------------------------------------------------------------------
# pull_files.sh
# Launcher for Watch Folder script
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2019 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYTHONPATH=$SCRIPTPATH

usage () {
    echo "Usage: ${0##*/} --from <user@machine:src-folder> --to <local-folder> --log <log-file> [--console] [-h | --help]"
    exit $1
}

FROM_FOLDER="/tmp"
TO_FOLDER="."
LOG_FILE=$$.log
CONSOLE=""

while true; do
    [ $# -lt 1 ] && break
    case $1 in
        --from)
            FROM_FOLDER=$2
            shift
            ;;
        --to)
            TO_FOLDER=$2
            shift
            ;;
        --log)
            LOG_FILE=$2
            shift
            ;;
        --console)
            CONSOLE="--console"
            ;;
        --help|-h)
            usage 1
            ;;
        *)
            usage 2
    esac
    shift
done

$PYTHON $SCRIPTPATH/watch_folder/watch_folder.py \
        -R ${FROM_FOLDER} -D ${TO_FOLDER} \
        -l ${LOG_FILE} ${CONSOLE}
